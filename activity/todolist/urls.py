from django.urls import path
from . import views

# Syntax
# path(route, view, name)
app_name = 'todolist'

urlpatterns = [
    path('', views.index, name ='index'),
    #/todolist/<todoitem_id>
    #/todolist/1
    # The <int:todoitem_id> allows for creating a dynamic link where the todoitem_id is provided
    path('<int:todoitem_id>', views.todoitem, name ='viewtodoitem'),
    # /todolist/register
    path('register', views.register, name='register'),
    path('update_profile', views.update_profile, name='update_profile'),
    path('login', views.login_view, name='login'),
    path('logout', views.logout_view, name='logout'),
    path('add_task', views.add_task, name='add_task'),
    # /todolist/<todoitem_id>/edit route
    path('<int:todoitem_id>/edit', views.update_task, name='update_task'),
    # /todolist/<todoitem_id>/delete route
    path('<int:todoitem_id>/delete', views.delete_task, name='delete_task'),
    # Event urls
    path('add_event', views.add_event, name='add_event'),
    path('events/<int:event_id>', views.event, name ='viewevent'),
]