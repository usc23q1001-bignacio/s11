from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponse
# from django.template import loader
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.forms.models import model_to_dict
from django.utils import timezone

from .models import ToDoItem, Events
from .forms import LoginForm, AddTaskForm, UpdateTaskForm, RegisterForm, UpdateUserForm, AddEventForm

def index(request):
    # todoitem_list = ToDoItem.objects.all()
    todoitem_list = ToDoItem.objects.filter(user_id=request.user.id)
    events_list = Events.objects.filter(user_id=request.user.id)

    context = {
        'todoitem_list': todoitem_list,
        'user' : request.user,
        'events_list' : events_list
    }

    return render(request, "todolist/index.html", context)

    # template = loader.get_template("todolist/index.html")
    # context = {
    #     'todoitem_list': todoitem_list
    # }
    # return HttpResponse(template.render(context, request))

    # output = ", ".join([todoitem.task_name for todoitem in todoitem_list])
    # return HttpResponse(output)

def todoitem(request, todoitem_id):

    # todoitem = model_to_dict(ToDoItem.objects.get(pk=todoitem_id))
    todoitem = get_object_or_404(ToDoItem, pk=todoitem_id)

    context = {
        "user" : request.user,
        "task_name" : todoitem.task_name,
        "todoitem_id" : todoitem.id,
        "description" : todoitem.description,
        "status" : todoitem.status,
    }

    # return render(request, "todolist/todoitem.html", todoitem)
    return render(request, "todolist/todoitem.html", context)

    # response = "You are viewing the details of %s"
    # return HttpResponse(response % todoitem_id)

def update_profile(request):

    context = {
        "user" : request.user,
    }

    if request.method == "POST":

        form = UpdateUserForm(request.POST)

        if form.is_valid() == False:

            form = UpdateUserForm()

        else:

            first_name = form.cleaned_data["first_name"]
            last_name = form.cleaned_data["last_name"]
            password = form.cleaned_data["password"]
            confirm_password = form.cleaned_data["confirm_password"]

            if password == confirm_password:
                current_user = User.objects.get(username=request.user.username)
                current_user.first_name = first_name
                current_user.last_name = last_name
                current_user.set_password(password)
                current_user.save()

                return redirect("todolist:login")
            else:
                error = True


    return render(request, "todolist/update_profile.html", context)

def add_task(request):

    context = {}

    if request.method == "POST":

        form = AddTaskForm(request.POST)

        if form.is_valid() == False:

            form = AddTaskForm()

        else:

            task_name = form.cleaned_data["task_name"] 
            description = form.cleaned_data["description"]

            # Checks the database if a task already exists
            # By default the "filter" method searches for records that are case insensitive 
            duplicates = ToDoItem.objects.filter(task_name=task_name, user_id=request.user.id)

            # if todoitem does not contain any duplicates
            if not duplicates:

                # Creates an object based on the "ToDoItem" model and saves the record in the database
                ToDoItem.objects.create(task_name=task_name, description=description, date_created=timezone.now(), user_id=request.user.id)
                return redirect("todolist:index")

            else:
                context = {
                    "error": True
                }

    return render(request, "todolist/add_task.html", context)

def update_task(request, todoitem_id):
    #Returns a queryset
    todoitem = ToDoItem.objects.filter(pk=todoitem_id)

    context = {
        "user": request.user,
        "todoitem_id": todoitem_id,
        # Accessing the first element is necessary because the "ToDoItem.objects.filter()" method returns a queryset
        "task_name": todoitem[0].task_name,
        "description": todoitem[0].description,
        "status": todoitem[0].status
    }
    
    if request.method == "POST":

        form = UpdateTaskForm(request.POST)

        if form.is_valid() == False:

            form = UpdateTaskForm()

        else:
            task_name = form.cleaned_data['task_name']
            description = form.cleaned_data['description'] 
            status = form.cleaned_data['status']
        
        if todoitem:
            todoitem[0].task_name = task_name 
            todoitem[0].description = description 
            todoitem[0].status = status

            todoitem[0].save()
            return redirect("todolist:index")

        else:
            context = {
                "error": True
            }

    return render(request, "todolist/update_task.html", context)

def delete_task(request, todoitem_id):
    todoitem = ToDoItem.objects.filter(pk=todoitem_id).delete()
    return redirect("todolist:index")

def add_event(request):

    context = {}

    if request.method == "POST":

        form = AddEventForm(request.POST)

        if form.is_valid() == False:

            form = AddEventForm()

            context = {
                "error": True,
                "error_message": "Form invalid. Please try again" 
            }

        else:

            event_name = form.cleaned_data["event_name"] 
            description = form.cleaned_data["description"]
            event_date = form.cleaned_data["event_date"]

            duplicates = Events.objects.filter(event_name=event_name, user_id=request.user.id)

            if not duplicates:
            
                Events.objects.create(event_name=event_name, description=description, event_date=event_date, user_id=request.user.id)
                return redirect("todolist:index")

            else:
                context = {
                    "error": True,
                    "error_message": "Duplicate event found. Please change event name" 
                }

    return render(request, "todolist/add_event.html", context)

def event(request, event_id):

    # todoitem = model_to_dict(ToDoItem.objects.get(pk=todoitem_id))
    event = get_object_or_404(Events, pk=event_id)

    context = {
        "user" : request.user,
        "event_name" : event.event_name,
        "event_id" : event.id,
        "description" : event.description,
        "status" : event.status,
        "event_date" : event.event_date
    }

    # return render(request, "todolist/todoitem.html", todoitem)
    return render(request, "todolist/event.html", context)

def login_view(request):

    context = {}

    # If this is a POST request we need to process the form data
    if request.method == 'POST':

        # Create a form instance and populate it with data from the request
        form = LoginForm(request.POST)
        # Check whether the data is valid
        # Runs validation routines for all the form fields and returns True and places the form's data in the "cleaned_data" attribute
        if form.is_valid() == False:
            # Returns a blank login form
            form = LoginForm()
        else:
        
            # Retrieves the information from the form 
            username = form.cleaned_data['username'] 
            password = form.cleaned_data['password']
            user = authenticate(username=username, password=password)
            context = {
                "username": username,
                "password": password
            }

        if user is not None:
            # Saves the user's ID in the session using Django's session framework 
            login(request, user)
            return redirect("todolist:index")
        else:
            # Provides context with error to conditionally render the error message 
            context = {
                "error": True
            }
    return render(request, "todolist/login.html", context)

def register(request):

    context = {}

    if request.method == 'POST':

        form = RegisterForm(request.POST)

        if form.is_valid() == False:
            form = RegisterForm()
        else:
            username = form.cleaned_data['username'] 
            password = form.cleaned_data['password']
            confirm_password = form.cleaned_data['confirm_password']
            first_name = form.cleaned_data['first_name']
            last_name = form.cleaned_data['last_name']
            email = form.cleaned_data['email']

        if password == confirm_password:

            users = User.objects.all()
            is_user_registered = False

            context = {
                "is_user_registered" : is_user_registered
            }

            for indiv_users in users:
                if indiv_users.username == username:
                    is_user_registered = True
                    break

            if is_user_registered == False:
                user = User()
                user.username = username
                user.first_name = first_name
                user.last_name = last_name
                user.email = email
                user.set_password(password)
                user.is_staff = False
                user.is_active = True
                user.save()
                context = {
                    "first_name" : user.first_name,
                    "last_name" : user.last_name
                }
            return redirect("todolist:login")
        else:
            context = {
                "error" : True
            }

    return render(request, "todolist/register.html", context)

def logout_view(request):
    logout(request)
    return redirect("todolist:index")